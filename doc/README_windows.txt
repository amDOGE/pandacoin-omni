Pandacoin 4.10.4 BETA

Copyright (c) 2014-2021 Pandacoin Developers
Copyright (c) 2011-2021 Peercoin Developers
Distributed under the MIT software license, see the accompanying
file COPYING or http://www.opensource.org/licenses/mit-license.php.
This product includes software developed by the OpenSSL Project for use in
the OpenSSL Toolkit (http://www.openssl.org/).  This product includes
cryptographic software written by Eric Young (eay@cryptsoft.com).


Intro
-----
Pandacoin is a free open source project derived from Bitcoin, with
the goal of providing a long-term energy-efficient crypto-currency.
Built on the foundation of Bitcoin, innovations such as proof-of-stake
help further advance the field of crypto-currency.


Setup
-----
Unpack the files into a directory and run pandacoin-qt.exe.

Pandacoin Core is the original Pandacoin client and it builds the backbone of the network.
However, it downloads and stores the entire history of Pandacoin transactions;
depending on the speed of your computer and network connection, the synchronization
process can take anywhere from a few hours to a day or more.

See the pandacoin documentation at:
  https://wiki.pandacoin.tech/
for more help and information.
